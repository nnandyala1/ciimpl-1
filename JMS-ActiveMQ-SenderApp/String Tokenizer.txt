import java.util.StringTokenizer;

public class StringTokenExample {
	public static void main(String[] args) {
		
		String Countrycode = "Tokenizer";
		String str = "This is String|Tokenizer|Example";
		StringTokenizer st2 = new StringTokenizer(str, "|");
		while (st2.hasMoreElements()) 
		{
			String token = st2.nextToken();
			if (Countrycode.equalsIgnoreCase(token))
			{
			System.out.println("Match Found");
			}
		}
	}
}